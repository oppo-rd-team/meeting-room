package com.oppo.meetingroom.domain

import com.google.gson.GsonBuilder
import com.oppo.meetingroom.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Remote {
    private var BASE_URL = BuildConfig.BASE_URL
    var token: String? = null
    val client: OkHttpClient =
            OkHttpClient().newBuilder()
        .readTimeout(5, TimeUnit.MINUTES)
        .writeTimeout(5, TimeUnit.MINUTES)
        .addInterceptor(HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        })
        .addInterceptor {
            var request = if(token != null){
                it.request().newBuilder()
                        .header("Authorization", "Bearer $token")
                        .header("source", "metting_room")
                        .addHeader("Accept", "application/json")
                        .addHeader("User-Agent", "android/${BuildConfig.VERSION_NAME}")
            }else{
                it.request().newBuilder()
                        .header("source", "metting_room")
                        .addHeader("Accept", "application/json")
                        .addHeader("User-Agent", "android/${BuildConfig.VERSION_NAME}")
            }
            return@addInterceptor it.proceed(request.build())
        }.addInterceptor {
                val response = it.proceed(it.request())
//                Log.d("tokendd",response.code().toString())
//                Log.d("tokendd",response.body().toString())
                return@addInterceptor  response
            }.build()

    fun builder(url: String): Retrofit{
        BASE_URL = url
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
            .build()
    }
}




