package com.oppo.meetingroom.domain.request

import com.google.gson.annotations.SerializedName

data class BookingRequest(
        @SerializedName("room_id")
        val roomId: Int,
        @SerializedName("title")
        val title: String,
        @SerializedName("datetime_from")
        val datetimeFrom: String,
        @SerializedName("datetime_to")
        val datetimeTo: String,
        @SerializedName("phone")
        val phone: String,
        @SerializedName("staff_code")
        val staffCode: String
)