package com.oppo.meetingroom.domain.model

data class Auth(
        val token: String
)