package com.oppo.meetingroom.domain.preferences

import android.content.Context
import com.google.gson.Gson
import com.oppo.meetingroom.domain.model.User

class UserPreference(context: Context) {
    private val preference = context.getSharedPreferences("user", Context.MODE_PRIVATE)

    fun isUser(): Boolean {
        return getUser() != null
    }

    fun getUser(): User? {
        val authJson = preference.getString("user", null)
        authJson?.let {
            return Gson().fromJson(authJson, User::class.java)
        }
        return null
    }

    fun setAuth(room: User) {
        preference.edit()
                .putString("user", Gson().toJson(room))
                .apply()
    }

    fun clear() {
        preference.edit().clear().apply()
    }
}