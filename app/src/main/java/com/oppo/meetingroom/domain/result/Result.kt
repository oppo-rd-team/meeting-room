package com.oppo.meetingroom.domain.result

import com.google.gson.annotations.SerializedName

data class Result<T>(
	val data: T? = null,
	@SerializedName("message_th")
	val messageTh: String? = null,
	@SerializedName("message_en")
	val messageEn: String? = null,
	val status: Int
)


