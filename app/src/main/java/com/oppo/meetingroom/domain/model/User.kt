package com.oppo.meetingroom.domain.model

import com.google.gson.annotations.SerializedName

data class User(
    val source: String = "metting_room",
    @SerializedName("staff_code")
    val staffCode: String = "9999000",
    val password: String = "U3VwZXJHcmVhbklUMjAyNSNA",
    val token: String? = null
)