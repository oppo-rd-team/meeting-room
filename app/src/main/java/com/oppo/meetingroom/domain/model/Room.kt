package com.oppo.meetingroom.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Room(

	@field:SerializedName("area")
	val area: String,

	@field:SerializedName("part_img")
	val partImg: String,

	@field:SerializedName("room_name")
	val roomName: String,

	@field:SerializedName("room_infor")
	val roomInfor: String,

	@field:SerializedName("img_all")
	val imgAll: String,

	@field:SerializedName("room_size")
	val roomSize: String,

	@field:SerializedName("room_infor_eng")
	val roomInforEng: String,

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("amount_chairs")
	val amountChairs: Int,

	@field:SerializedName("room_photo")
	val roomPhoto: String
) : Parcelable
