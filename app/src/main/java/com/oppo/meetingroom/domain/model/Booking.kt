package com.oppo.meetingroom.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Booking(
	@field:SerializedName("room_id")
	val roomId: Int,

	@field:SerializedName("note")
	val note: String,

	@field:SerializedName("firstname")
	val firstname: String,

	@field:SerializedName("tel_number")
	val telNumber: String,

	@field:SerializedName("datetime_create")
	val datetimeCreate: String,

	@field:SerializedName("repeat_tax")
	val repeatTax: String? = null,

	@field:SerializedName("department_id")
	val departmentId: Int,

	@field:SerializedName("origin")
	val origin: Int,

	@field:SerializedName("department_name")
	val departmentName: String,

	@field:SerializedName("staff_code")
	val staffCode: String,

	@field:SerializedName("avatar")
	val avatar: String,

	@field:SerializedName("title")
	val title: String,

	@field:SerializedName("datetime_from")
	val datetimeFrom: String,

	@field:SerializedName("deleted_at")
	val deletedAt: String? = null,

	@field:SerializedName("datetime_to")
	val datetimeTo: String,

	@field:SerializedName("lastname")
	val lastname: String,

	@field:SerializedName("room_name")
	val roomName: String,

	@field:SerializedName("user_code")
	val userCode: Int,

	@field:SerializedName("classname")
	val classname: String,

	@field:SerializedName("updated_at")
	val updatedAt: String,

	@field:SerializedName("phone")
	val phone: String,

	@field:SerializedName("nick_name")
	val nickName: String,

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("borrow_tool")
	val borrowTool: Int
) : Parcelable
