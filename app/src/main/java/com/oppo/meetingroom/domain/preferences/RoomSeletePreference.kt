package com.oppo.meetingroom.domain.preferences

import android.content.Context
import com.google.gson.Gson
import com.oppo.meetingroom.domain.model.Room

class RoomSeletePreference(context: Context) {
    private val preference = context.getSharedPreferences("room", Context.MODE_PRIVATE)

    fun isRoom(): Boolean {
        return getRoom() != null
    }

    fun getRoom(): Room? {
        val authJson = preference.getString("room", null)
        authJson?.let {
            return Gson().fromJson(authJson, Room::class.java)
        }
        return null
    }

//    fun getHouseId(): String?{
//        val house = getAuth()
//        house?.let {
//            return it.id
//        }
//        return null
//    }

    fun setRoom(room: Room) {
        preference.edit()
            .putString("room", Gson().toJson(room))
            .apply()
    }

    fun clear() {
        preference.edit().clear().apply()
    }
}