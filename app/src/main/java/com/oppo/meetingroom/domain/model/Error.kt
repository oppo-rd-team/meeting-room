package com.oppo.meetingroom.domain.model

import com.google.gson.annotations.SerializedName

data class Error(
    val status: String,
    @SerializedName("message_en")
    val messageEn:String,
    @SerializedName("message_th")
    val messageTh:String

)
