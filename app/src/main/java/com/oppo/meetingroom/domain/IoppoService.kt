package com.oppo.meetingroom.domain

import com.oppo.meetingroom.domain.model.Auth
import com.oppo.meetingroom.domain.model.Booking
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.request.BookingRequest
import com.oppo.meetingroom.domain.result.Result
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface IoppoService {
    @GET("hr/list-room-all")
    fun fetchRoom(
//            @Header("Authorization") token: String
    ): Single<List<Room>>

    @GET("hr/list-booking-room")
    fun fetchBooking(
        @Query("select_date") selectDate: String,
        @Query("room_id") roomId: Int
    ) : Single<List<Booking>>

    @GET("hr/booking-active")
    fun fetchBookingActive(
            @Query("datetime") dateTime: String,
            @Query("room_id") roomId: Int
    ): Single<List<Booking>>

    @POST("hr/create-booking-room")
    fun createBooking(
            @Body booking: BookingRequest
    ): Flowable<Result<List<Booking>>>

    @POST("login")
    fun login(
            @Query("source") source: String,
            @Query("staff_code") staffCode: String,
            @Query("password") password: String
    ): Single<Auth>
}