package com.oppo.meetingroom.di

import com.oppo.meetingroom.BuildConfig
import com.oppo.meetingroom.domain.IoppoService
import com.oppo.meetingroom.domain.Remote
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.domain.preferences.UserPreference
import com.oppo.meetingroom.ui.booking.BookingViewModel
import com.oppo.meetingroom.ui.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val appModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { BookingViewModel(get(), androidContext()) }
}

val remote : Retrofit = Remote.builder(BuildConfig.BASE_URL)
val ioppoService: IoppoService = remote.create(IoppoService::class.java)


val networkModule = module {
    single { ioppoService }
}

val prefModule = module {
    single { RoomSeletePreference(androidContext()) }
    single { UserPreference(androidContext()) }
}