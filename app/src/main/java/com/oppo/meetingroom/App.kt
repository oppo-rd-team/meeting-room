package com.oppo.meetingroom

import android.app.Application
import androidx.multidex.MultiDex
import com.oppo.meetingroom.di.appModule
import com.oppo.meetingroom.di.networkModule
import com.oppo.meetingroom.di.prefModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    init {
        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(arrayListOf(appModule, networkModule, prefModule))
        }
    }

}