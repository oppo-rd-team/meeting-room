package com.oppo.meetingroom.ui.components

import android.app.Dialog
import android.os.Bundle
import android.os.Message
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.DialogBasicBinding
import java.lang.IllegalStateException


class BaseDialogFragment : DialogFragment() {

    private var positiveClick: ((view: View) -> Unit)? = null
    private var negativeClick: ((view: View) -> Unit)? = null

    private var title: String? = null
    private var message: String = ""

    private var negativeEnable = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreen)
        retainInstance = true
    }

    private val binding: DialogBasicBinding by lazy {
        DialogBasicBinding.inflate(layoutInflater)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
//        dialog.window?.apply {
////            requestFeature(Window.FEATURE_NO_TITLE)
//            setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT)
//        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            title = it.getString(ARG_TITLE)
            message = it.getString(ARG_MESSAGE)!!
        }

        binding.message.text = message


        binding.close.setOnClickListener {
            positiveClick?.invoke(it)
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    fun positiveClick(positive: ((view: View) -> Unit)){
        positiveClick = positive
    }

    fun negativeClick(click: ((view: View) -> Unit)) {
        negativeClick = click
        negativeEnable = true
    }

    fun show(fragmentManager: FragmentManager){
        show(fragmentManager, BaseDialogFragment::class.java.simpleName)
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            val ft = manager.beginTransaction()
            ft.add(this,tag)
            ft.commitAllowingStateLoss()
        } catch (e: IllegalStateException){
            e.printStackTrace()
        }
    }

    override fun onDestroyView() {
        if(dialog != null && retainInstance){
            dialog?.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    companion object{
        const val ARG_TITLE = "title"
        const val ARG_MESSAGE = "message"

        fun newInstance(title: String?, message: String): BaseDialogFragment {
            return BaseDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TITLE,title)
                    putString(ARG_MESSAGE,message)
                }
            }
        }
    }

}