package com.oppo.meetingroom.ui.booking

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.oppo.meetingroom.R
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.ui.main.MainActivity
import com.oppo.meetingroom.utli.ext.replaceFragment
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class RoomActivity : AppCompatActivity() {

    private val viewModel: BookingViewModel by viewModel()
    private val roomSeletePreference : RoomSeletePreference by inject()
    private lateinit var room: Room
    private var currentApiVersion = 0
    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentApiVersion = Build.VERSION.SDK_INT

//        val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                or View.SYSTEM_UI_FLAG_FULLSCREEN
////                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                )
//
//        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {
//            window.decorView.systemUiVisibility = flags
//            val decorView = window.decorView
//            decorView
//                    .setOnSystemUiVisibilityChangeListener { visibility ->
//                        if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
//                            decorView.systemUiVisibility = flags
//                        }
//                    }
//        }

//       if (Build.VERSION.SDK_INT < 16) {
//            window.setFlags(
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
//            window.setFlags(
//                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
//            )
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            window.apply {
////                clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
////                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    decorView.systemUiVisibility = (
//                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//                            )
//                } else {
//                    decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                }
//            }
//        }

        // Hide the status bar.
//        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
//        actionBar?.hide()

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            val window = window
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            window.statusBarColor = Color.TRANSPARENT
//        }

//        window.apply {
//            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                statusBarColor = Color.TRANSPARENT
//            }
//        }

        setContentView(R.layout.activity_room)

        init()
        initView()

    }

    private fun init() {
        roomSeletePreference.getRoom()?.let {
            this.room = it
        }

//        val oriString = "bezkoder tutorial"
//        val encodedString: String = Base64.encodeToString(oriString.toByteArray(),Base64.NO_PADDING)
//
//        val data: ByteArray = Base64.decode(encodedString, Base64.NO_PADDING)
//        val text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//          val st =  String(data, StandardCharsets.UTF_8)
//            Log.d("dfd",st)
//        } else {
//
//        }

    }

    override fun onStart() {
        super.onStart()
        handler.post(object : Runnable {
            override fun run() {
                handler.postDelayed(this, 30000)
                viewModel.fetchBookingActive(this@RoomActivity, room)
            }
        })
    }

    private fun initView() {
//        viewModel.booking.observe(this, Observer {
//            when(it){
//                is Response.Success -> {
//                    if(it.data.isEmpty()){
////                        containerRight.visibility = View.GONE
//                    }else{
//                        containerRight.visibility = View.VISIBLE
//                    }
//                }
//                is Response.Empty -> {
//                    containerRight.visibility = View.GONE
//                }
//                else -> {}
//            }
//        })

        replaceFragment(R.id.containerLeft, ::RoomForeFragment)
        replaceFragment(R.id.containerRight, ::RoomBookingListFragment)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.option, menu);
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.admin -> {
                MainActivity.newInstant(this)
                finish()
                true
            }
            R.id.bookingRoom -> {

                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        handler.removeMessages(0)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            val decorView = window.decorView
            decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        }
    }

    companion object{
        fun newInstant(context: Context){
            context.startActivity(Intent(context, RoomActivity::class.java))
        }
    }
}