package com.oppo.meetingroom.ui.main.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.ViewItemRoomBinding
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.ui.main.MainViewModel


class RoomAdapter(
    private val context: Context,
    private val viewModel: MainViewModel
): RecyclerView.Adapter<RoomAdapter.RoomViewHolder>(){
    private var list: List<Room> = emptyList()

    fun setData(list: List<Room>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.view_item_room, parent, false)
        return RoomViewHolder(ViewItemRoomBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        holder.bind(list[position],viewModel)
    }

    inner class RoomViewHolder(var binding: ViewItemRoomBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Room, viewModel: MainViewModel){
//            Picasso.get().load(item.roomPhoto).into(object : Target)

//            Glide.with(itemView.context)
//                .load(item.roomPhoto)
//                .into(itemView.image)

            Glide.with(context)
                            .asBitmap()
//                            .apply(RequestOptions().override(100,100))
                            .load(item.roomPhoto)
                            .into(object : CustomTarget<Bitmap>(){
                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                    binding.image.setImageBitmap(resource)
                                }

                                override fun onLoadCleared(placeholder: Drawable?) {

                                }
                            })


//            Glide.with(context).asBitmap()
//                .load("http://meetingroom.oppo.in.th/images/koh/phiphi/Phi-Phi.webp")
//                .addListener(object :RequestListener<Bitmap> {
//                    override fun onLoadFailed(
//                        e: GlideException?,
//                        model: Any?,
//                        target: Target<Bitmap>?,
//                        isFirstResource: Boolean
//                    ): Boolean {
//                        return false
//                    }
//
//                    override fun onResourceReady(
//                        resource: Bitmap?,
//                        model: Any?,
//                        target: Target<Bitmap>?,
//                        dataSource: DataSource?,
//                        isFirstResource: Boolean
//                    ): Boolean {
////                        val canvas = resource?.let { Canvas(it) }
//                        itemView.image.setImageBitmap(resource)
//                        return false;
//                    }
//
//                }).submit(60,60)



//            Picasso.get().load(item.roomPhoto).into(object : Target {
//                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
//                    itemView.image.background = BitmapDrawable(context.resources, bitmap)
//                }
//                override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {}
//                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
//                    itemView.image.background = placeHolderDrawable
//                }
//            })

            binding.root.setOnClickListener {
                viewModel.roomSelect(item)
            }
            binding.title.text = item.roomName
        }
    }

}