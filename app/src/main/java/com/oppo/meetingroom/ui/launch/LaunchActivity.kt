package com.oppo.meetingroom.ui.launch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.lifecycle.Observer
import com.oppo.meetingroom.R
import com.oppo.meetingroom.domain.Remote
import com.oppo.meetingroom.domain.model.User
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.domain.preferences.UserPreference
import com.oppo.meetingroom.domain.result.Response
import com.oppo.meetingroom.ui.booking.RoomActivity
import com.oppo.meetingroom.ui.main.MainActivity
import com.oppo.meetingroom.ui.main.MainViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class LaunchActivity : AppCompatActivity() {
    private val roomSeletePreference: RoomSeletePreference by inject()
    private val userPreference: UserPreference by inject()
    private val viewModel: MainViewModel by viewModel()

    companion object{
        private const val  LAUNCH_DELAY: Long = 1500
    }
    private var mDelayHandler: Handler? = null
    private var countDown: Long = LAUNCH_DELAY

    private val mRunnable = Runnable {
        if(roomSeletePreference.isRoom()){
            RoomActivity.newInstant(this)
            finish()
        }else{
            MainActivity.newInstant(this)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        observer()
        checkLogin()

        mDelayHandler = Handler()
//         roomSeletePreference.clear()
//        userPreference.clear()
    }

    private fun checkLogin() {
        if(userPreference.isUser()){
            userPreference.getUser()?.let {
                Remote.token = it.token
            }
        }else{
            viewModel.login(User())
        }
    }

    private fun observer() {
        viewModel.auth.observe(this, Observer {
            when(it){
                is Response.Success -> {
                    Remote.token = it.data.token
                    userPreference.setAuth(User(token = it.data.token))
                }
                is Response.Error -> {
                    Log.d("test","error")
                }
                else -> {
                    Log.d("test","error")
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        mDelayHandler?.postDelayed(mRunnable,countDown)
        countDown += System.currentTimeMillis()
    }

    override fun onStop() {
        super.onStop()
        countDown -= System.currentTimeMillis()
        mDelayHandler?.removeCallbacks(mRunnable)
    }
}