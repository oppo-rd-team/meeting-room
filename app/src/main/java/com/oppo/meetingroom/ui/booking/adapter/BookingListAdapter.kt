package com.oppo.meetingroom.ui.booking.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.ViewItemBookingBinding
import com.oppo.meetingroom.databinding.ViewItemButtonBookingBinding
import com.oppo.meetingroom.databinding.ViewTimeBookingActiveBinding
import com.oppo.meetingroom.domain.model.Booking
import com.oppo.meetingroom.ui.booking.BookingViewModel
import com.oppo.meetingroom.ui.booking.RoomBookingFragment
import com.oppo.meetingroom.utli.UtilDate
import com.oppo.meetingroom.utli.ext.replaceAddFragment
import java.util.*

class BookingListAdapter(
    private val context: Context,
    private val viewModel: BookingViewModel
): RecyclerView.Adapter<BookingViewHolder>() {
    private var list: List<Any> = emptyList()
    private var active = false

    fun setData(list: List<Booking>, active:Boolean){
        var data = mutableListOf<Any>()
        data.addAll(list)
        this.list = data
        this.active = active
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingViewHolder {
        return when(viewType){
            ITEM_ACTIVE -> {
                val itemView = ViewTimeBookingActiveBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//                val itemView = LayoutInflater.from(parent.context).inflate(
//                    R.layout.view_time_booking_active,
//                    parent,
//                    false
//                )
                BookingViewHolder.itemActive(itemView)
            }
            BOOKING -> {
                val itemView = ViewItemButtonBookingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//                val itemView = LayoutInflater.from(parent.context).inflate(
//                        R.layout.view_item_button_booking,
//                        parent,
//                        false
//                )
                BookingViewHolder.ItemBooking(itemView)
            }
             else -> {
                 val itemView = ViewItemBookingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//                 val itemView = LayoutInflater.from(parent.context).inflate(
//                     R.layout.view_item_booking,
//                     parent,
//                     false
//                 )
                 BookingViewHolder.itemInActive(itemView)
            }
        }
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: BookingViewHolder, position: Int) {
        when(holder.itemViewType){
            ITEM_ACTIVE -> {
                val item = holder as BookingViewHolder.itemActive
                item.bind(list[position] as Booking)
            }
            BOOKING -> {
                val item = holder as BookingViewHolder.ItemBooking
                item.bind(context)
            }
            else -> {
                val item = holder as BookingViewHolder.itemInActive
                item.bind(list[position] as Booking)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if(list[position] is Booking){
            if(position == 0 && active){
                ITEM_ACTIVE
            }
            else{
                ITEM_IN_ACTIVE
            }
        }else{
            BOOKING
        }

    }

    companion object{
        const val ITEM_ACTIVE = 0
        const val ITEM_IN_ACTIVE = 1
        const val BOOKING = 2
    }
}

sealed class BookingViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    class itemActive(var binding: ViewTimeBookingActiveBinding): BookingViewHolder(binding.root){
        @SuppressLint("SetTextI18n")
        fun bind(item: Booking){
            var startTime = UtilDate.coverDateToTime(item.datetimeFrom)
            var endTime = UtilDate.coverDateToTime(item.datetimeTo)

            binding.apply {

                title.text = item.title
                time.text = "$startTime - $endTime"
                staff.text = "${item.firstname} ${item.lastname}  (${item.userCode})"
                department.text = item.departmentName
                if(countDayTime(item.datetimeTo, item.datetimeFrom) > 0){
                    tvDate.text = "${UtilDate.coverDateTimeToDDMMYY(item.datetimeFrom)} - ${UtilDate.coverDateTimeToDDMMYY(item.datetimeTo)}"
                }else{
                    tvDate.text = UtilDate.coverDateTimeToDDMMYY(item.datetimeFrom)
                }
                tvPhone.text = item.phone
            }
        }
    }
    class itemInActive(var binding: ViewItemBookingBinding): BookingViewHolder(binding.root){
        @SuppressLint("SetTextI18n")
        fun bind(item: Booking){
            var startTime = UtilDate.coverDateToTime(item.datetimeFrom)
            var endTime = UtilDate.coverDateToTime(item.datetimeTo)
            binding.apply {
                title.text = item.title
                time.text = "$startTime - $endTime"
                staff.text = "${item.firstname} ${item.lastname}  (${item.userCode})"
                department.text = item.departmentName
                if(countDayTime(item.datetimeTo, item.datetimeFrom) > 0){
                    tvDate.text = "${UtilDate.coverDateTimeToDDMMYY(item.datetimeFrom)} - ${UtilDate.coverDateTimeToDDMMYY(item.datetimeTo)}"
                }else{
                    tvDate.text = UtilDate.coverDateTimeToDDMMYY(item.datetimeFrom)
                }
                tvPhone.text = item.phone
            }
        }
    }

    class ItemBooking(var binding: ViewItemButtonBookingBinding): BookingViewHolder(binding.root){
        fun bind(context: Context){
            binding.apply {
                floatingActionButton.setOnClickListener {
                    val active = context as AppCompatActivity
                    active.apply {
                        if(supportFragmentManager.backStackEntryCount < 1){
                            replaceAddFragment(R.id.containerLeft,::RoomBookingFragment)
                        }
                    }
                }
            }
        }
    }

    fun countDayTime(endDate: String, startDate: String): Long {
        // Set Current Date
        val currentDate = UtilDate.getDataCalendar(startDate)
        // Set Event Date
        val eventDate = UtilDate.getDataCalendar(endDate)
        // Find how many milliseconds until the event
        val diff = eventDate.timeInMillis - currentDate.timeInMillis
        val days = diff / (24 * 60 * 60 * 1000)

        return days
    }
}