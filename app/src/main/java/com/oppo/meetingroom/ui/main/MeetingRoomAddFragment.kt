package com.oppo.meetingroom.ui.main

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.FragmentMeetingRoomAddBinding
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.ui.booking.RoomActivity
import com.oppo.meetingroom.utli.Util
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class MeetingRoomAddFragment : Fragment() {
    private lateinit var binding: FragmentMeetingRoomAddBinding
    private val viewModel: MainViewModel by sharedViewModel()
    private val roomSeletePreference: RoomSeletePreference by inject()
    private var room: Room? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_meeting_room_add, container, false)
        observer()
        initView()
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initView() {
        showRoom()
        binding.btAdd.setOnClickListener {
            room?.let { it1 -> roomSeletePreference.setRoom(it1) }
            it.isEnabled = false
            activity?.apply {
                RoomActivity.newInstant(this)
                finish()
            }
        }
    }

    private fun showRoom() {
        if(roomSeletePreference.isRoom()){
            room = roomSeletePreference.getRoom()
            room?.let {
                binding.apply {
                    materialCardView.visibility = View.VISIBLE
                    title.text = it.roomName
                    setImage(fr, it)
                }
            }
        }
    }

    private fun observer() {
        viewModel.room.observe(viewLifecycleOwner, Observer {
            room = it
            binding.materialCardView.visibility = View.VISIBLE
            binding.btAdd.isEnabled = true
            binding.title.text = it.roomName

//            Log.d("TAG", "=> path photo : ${it.roomPhoto}")
            Glide.with(requireContext())
                .asBitmap()
//                            .apply(RequestOptions().override(100,100))
                .load(it.roomPhoto)
                .into(object : CustomTarget<Bitmap>(){
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        binding.imgRoom.setImageBitmap(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }
                })

//            Util.imageNoView(requireContext(), it.roomPhoto, binding.imgRoom)
//            setImage(binding.fr,it)
        })
    }

    private fun setImage(view: View, item: Room){


//        Picasso.get().load(item.roomPhoto).into(object : Target {
//            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
//                view.background = BitmapDrawable(this@MeetingRoomAddFragment.resources, bitmap)
//            }
//            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {}
//            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
//                view.background = placeHolderDrawable
//            }
//        })
    }

    companion object {

        @JvmStatic
        fun newInstance() = MeetingRoomAddFragment()
    }
}