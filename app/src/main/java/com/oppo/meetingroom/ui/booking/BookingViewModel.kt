package com.oppo.meetingroom.ui.booking

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.oppo.meetingroom.base.BaseViewModel
import com.oppo.meetingroom.di.ioppoService
import com.oppo.meetingroom.domain.model.Booking
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.preferences.UserPreference
import com.oppo.meetingroom.domain.request.BookingRequest
import com.oppo.meetingroom.domain.result.Response
import com.oppo.meetingroom.utli.UtilDate
import com.oppo.meetingroom.utli.ext.handleErrorBody
import com.oppo.meetingroom.utli.ext.refreshToken
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*

class BookingViewModel(
    private val userPreference: UserPreference,
    private val context: Context
) : BaseViewModel() {
    private val _booking = MutableLiveData<Response<List<Booking>>>()
    val booking: LiveData<Response<List<Booking>>>
        get() = _booking

    private val _bookingActive = MutableLiveData<Response<Booking>>()
    val bookingActivity: LiveData<Response<Booking>>
        get() = _bookingActive

    private val _bookingRoom = MutableLiveData<Response<Booking>>()
    val bookingRoom: LiveData<Response<Booking>>
        get() = _bookingRoom

    fun fetchListBooking(room: Room, context: Context) {

        val disposable = ioppoService.fetchBooking(UtilDate.getDataNow(), room.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _booking.postValue(Response.Success(it))
//                if(it.isEmpty()){
//                    _booking.postValue(Response.Empty)
//                }else{
//                    _booking.postValue(Response.Success(it))
//                }

            }, {
                refreshToken(ioppoService, userPreference, it, {
                    fetchListBooking(room, context)
                }, {
                    _booking.postValue(Response.Error(it.handleErrorBody()))
                }, context)
            })
        addDisposable(disposable)

    }

    fun fetchBookingActive(context: Context? = null, room: Room) {
        val disposable = ioppoService.fetchBookingActive(UtilDate.getDataTimeNow(), room.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isNotEmpty()) {
                    _bookingActive.postValue(Response.Success(it[0]))
                } else {
                    _bookingActive.postValue(Response.Empty)
                }
                fetchListBooking(room, context!!)
            }, {
                refreshToken(ioppoService, userPreference, it, {
                    fetchBookingActive(context, room)
                }, {
                    _bookingActive.postValue(Response.Error(it.handleErrorBody()))
                }, context!!)
            })
        addDisposable(disposable)

    }

    fun bookingRoom(bookingRequest: BookingRequest) {
        _bookingRoom.postValue(Response.Loading)
        val log : List<Booking> = arrayListOf()
        val disposable = ioppoService.createBooking(bookingRequest)
            .subscribeOn(Schedulers.io())                          // ทำงานบน IO Thread
            .observeOn(AndroidSchedulers.mainThread())
            .map { response ->
//                arrayOf("", 1)// ใช้ map ใน RxJava3
                when (response.status) {
                    200 -> response.data
                    else -> {
                        val errorMessage = when (Locale.getDefault().language) {
                            "th" -> response.messageTh
                            else -> response.messageEn
                        }
                        throw Exception(errorMessage)
                    }
                }
                log

            }
            .subscribe({ data ->
                if (data.isNotEmpty()) {
                    _bookingRoom.postValue(Response.Success(data[0]))
                } else {
                    _bookingRoom.postValue(Response.Empty)
                }
            }, { throwable ->
                // Handle Token Refresh ใน RxJava3
                refreshToken(ioppoService, userPreference, throwable, {
                    bookingRoom(bookingRequest) // Retry หลังจาก Refresh Token
                }, {
                    _bookingRoom.postValue(Response.Error(it.handleErrorBody()))
                }, context)
            })
        addDisposable(disposable)

    }

    fun clearBookingRoom() {
        _bookingRoom.postValue(Response.Empty)
    }

    fun setBookingListEmpty() {
        _booking.postValue(Response.Empty)
    }

}