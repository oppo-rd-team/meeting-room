package com.oppo.meetingroom.ui.booking


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.CalendarConstraints.DateValidator
import com.google.android.material.datepicker.CompositeDateValidator
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.FragmentRoomBookingBinding
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.domain.request.BookingRequest
import com.oppo.meetingroom.domain.result.Response
import com.oppo.meetingroom.ui.components.BaseDialogFragment
import com.oppo.meetingroom.ui.components.CustomTimePicker
import com.oppo.meetingroom.utli.DialogLoading
import com.oppo.meetingroom.utli.Util
import com.oppo.meetingroom.utli.Util.Companion.hideSoftKeyboard
import com.oppo.meetingroom.utli.UtilDate
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*


class RoomBookingFragment : Fragment() {

    private lateinit var binding: FragmentRoomBookingBinding
    private val viewModel: BookingViewModel by sharedViewModel()
    private val roomSeletePreference: RoomSeletePreference by inject()
    private var dialogFragment: BaseDialogFragment? = null
    private lateinit var room: Room

    private var datedefault: Long = 0

    var isKeyboardShowing = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_room_booking,
            container,
            false
        )
        init()
        initView()
        listener()
        observer()

        return binding.root
    }

    private fun init() {
        roomSeletePreference.getRoom()?.let {
           this.room = it
        }
    }

    private fun observer() {
        viewModel.bookingRoom.observe(this, Observer {
            when (it) {
                is Response.Success -> {
                    DialogLoading.closeDialogLoading(requireContext())
                    viewModel.clearBookingRoom()
                    activity?.apply {
                        onBackPressed()
                    }
                }
                is Response.Error -> {
                    DialogLoading.closeDialogLoading(requireContext())
                    showDialogFragment("warning", it.exception.message.toString(), {
                        viewModel.clearBookingRoom()
                    }, {})
                }
                Response.Loading -> {
                    DialogLoading.showDialogLoading(requireContext())
                }
                else -> DialogLoading.closeDialogLoading(requireContext())
            }
        })
    }

    private fun listener() {
        binding.etDateStart.setOnClickListener {
            setDataPicker(binding.etDateStart)
        }

        binding.etDateEnd.setOnClickListener { setDataPicker(binding.etDateEnd) }

        binding.btBack.setOnClickListener { activity?.apply { onBackPressed() } }

        binding.btReserve.setOnClickListener {
            if(checkEdEmpty(binding.lyStaff, binding.etStaff, getString(R.string.invalid_employee))
                    && checkEdEmpty(
                    binding.lyTopic,
                    binding.textTopic,
                    getString(R.string.invalid_title)
                )
                    && binding.etStartTime.checkEdEmpty(getString(R.string.invalid_time))
                    && binding.etEndTime.checkEdEmpty(getString(R.string.invalid_time))
                    && checkEdEmpty(
                    binding.lyPhone,
                    binding.etPhone,
                    getString(R.string.invalid_phone_number)
                )
                    && checkCountPhone(
                    binding.etPhone,
                    binding.lyPhone,
                    "The phone number must not exceed 10 digits."
                )
                    && checkTimeStartOverCurrent()
                    && checkTime())
            {
                viewModel.bookingRoom(
                    BookingRequest(
                        room.id, binding.textTopic.text.toString(),
                        UtilDate.coverDateTimeToYMD(
                            binding.etDateStart.text.toString(),
                            binding.etStartTime.getValue()
                        ),
                        UtilDate.coverDateTimeToYMD(
                            binding.etDateEnd.text.toString(),
                            binding.etEndTime.getValue()
                        ),
                        binding.etPhone.text.toString(),
                        binding.etStaff.text.toString()
                    )
                )
            }
        }

        binding.etStartTime.onCallback(object : CustomTimePicker.TimePickerListener {
            override fun onSelected(time: String?) {

            }
        })

        binding.scrollView.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    activity?.let { hideSoftKeyboard(v, it) }
                }
            }
            false
        }
    }

    private fun initView() {
        binding.etDateStart.setText(UtilDate.getDataNowMM())
        binding.etDateEnd.setText(UtilDate.getDataNowMM())

        checkCountString(binding.etStaff,binding.lyStaff,1)
        checkCountString(binding.textTopic,binding.lyTopic,1)
        checkCountString(binding.etPhone,binding.lyPhone,10)

    }

    private fun setDataPicker(view: TextInputEditText){
//        val builder = MaterialDatePicker.Builder.datePicker()
//        val picker = builder.build()
//        picker.show(requireActivity().supportFragmentManager, picker.toString())
//        picker.addOnPositiveButtonClickListener {
//           view.setText(picker.headerText)
//        }

        val builder: MaterialDatePicker.Builder<Long> = setupDateSelectorBuilder()
        val constraintsBuilder: CalendarConstraints.Builder = setupConstraintsBuilder()

        builder.setTitleText("")
        try {
            builder.setCalendarConstraints(constraintsBuilder.build())
            val picker = builder.build()
            picker.addOnPositiveButtonClickListener { selectedDate -> // Create a date format, then a date object with our offset
                val simpleFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val date = Date(selectedDate)
                val datesp = simpleFormat.format(date).split("-".toRegex()).toTypedArray()
                val year = datesp[0]
                val month = datesp[1]
                val day = datesp[2]
//                defautDate = true
//                datedefault = selectedDate
//                setError(false)
//                datePickerListener.onSelected(
//                    simpleFormat.format(date).toString(),
//                    day,
//                    month,
//                    year
//                )
//                tipDate.setText(Utils.dateDisplay(context, simpleFormat.format(date), 2))
            }
            picker.show((context as AppCompatActivity).supportFragmentManager, picker.toString())
        } catch (e: IllegalArgumentException) {
            Toast.makeText(context, e.message.toString(), Toast.LENGTH_LONG).show()
        }
    }


    private fun setupDateSelectorBuilder(): MaterialDatePicker.Builder<Long> {
        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
        return builder
    }

    private fun setupConstraintsBuilder(): CalendarConstraints.Builder {
        val constraintsBuilder = CalendarConstraints.Builder()
        val validators: MutableList<DateValidator> = ArrayList()
//        if (true) {
            val lowerBoundCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            lowerBoundCalendar.add(
                Calendar.DAY_OF_MONTH,
                (Util.datediff(UtilDate.getDataNowYyyyMmDd(), Util.today()) - 1)
            )
            val dateValidatorMin: DateValidator =
                DateValidatorPointForward.from(lowerBoundCalendar.timeInMillis)
            validators.add(dateValidatorMin)
//        }
        constraintsBuilder.setValidator(CompositeDateValidator.allOf(validators))
        return constraintsBuilder
    }

    private fun checkEdEmpty(ly: TextInputLayout, et: TextInputEditText, textError: String): Boolean{
        if(et.text.isNullOrEmpty()){
            ly.isErrorEnabled = true
            ly.error = textError
            return false
        }
        ly.isErrorEnabled = false
        return true
    }

    private fun checkTimeStartOverCurrent(): Boolean{
        val startDate =  UtilDate.getDataCalendar(
            UtilDate.coverDateTimeToYMD(
                binding.etDateStart.text.toString(),
                binding.etStartTime.getValue()
            )
        )
        val currentDate = Calendar.getInstance()
        val diff = startDate.timeInMillis < currentDate.timeInMillis - 1800000
        if(diff){
            binding.etStartTime.setError(true, "Time Invalid.")
            return false
        }
        return true
    }

    private fun checkTime():Boolean{
        val startDate =  UtilDate.getDataCalendar(
            UtilDate.coverDateTimeToYMD(
                binding.etDateStart.text.toString(),
                binding.etStartTime.getValue()
            )
        )
        val endDate = UtilDate.getDataCalendar(
            UtilDate.coverDateTimeToYMD(
                binding.etDateEnd.text.toString(),
                binding.etEndTime.getValue()
            )
        )

        val diff = endDate.timeInMillis < startDate.timeInMillis
        if(diff){
            binding.etEndTime.setError(true, "Time invalid.")
            return false
        }
        return true
    }

    private fun checkCountPhone(
        te: TextInputEditText,
        ly: TextInputLayout,
        textError: String
    ): Boolean{
        return if (te.length() < 10) {
            ly.isErrorEnabled = true
            ly.error = textError
            false
        } else {
            ly.isErrorEnabled = false
            true
        }
    }

    private fun checkCountString(te: TextInputEditText, tl: TextInputLayout, countText: Int) {
        te.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
               s?.let {
                   if(it.length >= countText){
                       tl.isErrorEnabled = false
                   }
               }
            }
            override fun afterTextChanged(s: Editable?) {

            }

        })
    }

    fun showDialogFragment(
        title: String? = null,
        message: String,
        positive: (view: View) -> Unit,
        negative: ((view: View) -> Unit)? = null
    ): BaseDialogFragment? {
        dialogFragment = BaseDialogFragment.newInstance(title, message).apply {
            positiveClick {
                positive.invoke(it)
            }
            negative?.let {
                negativeClick {
                    negative.invoke(it)
                }
            }
        }
        activity?.supportFragmentManager?.let { dialogFragment?.show(it) }
        return dialogFragment
    }



}