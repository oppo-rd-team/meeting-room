package com.oppo.meetingroom.ui.main

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.oppo.meetingroom.base.BaseViewModel
import com.oppo.meetingroom.di.ioppoService
import com.oppo.meetingroom.domain.model.Auth
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.model.User
import com.oppo.meetingroom.domain.preferences.UserPreference
import com.oppo.meetingroom.domain.result.Response
import com.oppo.meetingroom.utli.DialogLoading
import com.oppo.meetingroom.utli.ext.handleErrorBody
import com.oppo.meetingroom.utli.ext.refreshToken
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class MainViewModel(
        private val userPreference: UserPreference
) : BaseViewModel() {

//    private val ioppoService = ioppoServiceTest.builder().create(IoppoService::class.java)

    private val _rooms = MutableLiveData<List<Room>>()
    val rooms: LiveData<List<Room>>
        get() = _rooms

    private val _room = MutableLiveData<Room>()
    val room: LiveData<Room>
        get() = _room

    private val _auth = MutableLiveData<Response<Auth>>()
    val auth: LiveData<Response<Auth>>
        get() = _auth

    fun fetchRoom(context: Context){
        DialogLoading.showDialogLoading(context)
        val disposable = ioppoService.fetchRoom()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _rooms.postValue(it)
                DialogLoading.closeDialogLoading(context)
            },{
                DialogLoading.closeDialogLoading(context)
                refreshToken(ioppoService,userPreference,it,{
                    fetchRoom(context)
                },{


                    Log.d("error", it.handleErrorBody().message.toString())
                }, context)
            })
//            .subscribe({
//                _rooms.postValue(it)
//                DialogLoading.closeDialogLoading(context)
//            },{
//                DialogLoading.closeDialogLoading(context)
//                refreshToken(ioppoService,userPreference,it,{
//                    fetchRoom(context)
//                },{
//                    Log.d("error","error")
//                })
//            })
        addDisposable(disposable)
    }

    fun  roomSelect(item: Room){
        _room.postValue(item)
    }

    fun login(data: User){
        val disposable = ioppoService.login(data.source,data.staffCode,data.password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _auth.postValue(Response.Success(it))
                },{
                    _auth.postValue(Response.Error(it.handleErrorBody()))
                })
        addDisposable(disposable)
    }
}