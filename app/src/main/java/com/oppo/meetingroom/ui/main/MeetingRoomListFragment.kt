package com.oppo.meetingroom.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.FragmentMeetingRoomListBinding
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.ui.main.adapter.RoomAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class MeetingRoomListFragment : Fragment() {
    private lateinit var binding: FragmentMeetingRoomListBinding
    private lateinit var roomAdapter: RoomAdapter
    private val viewModel: MainViewModel by sharedViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =  DataBindingUtil.inflate(inflater,R.layout.fragment_meeting_room_list, container, false)
        roomAdapter = RoomAdapter(requireContext(),viewModel)
        initView()
        observer()
        init()
        return binding.root
    }

    private fun init() {
        viewModel.fetchRoom(requireContext())
    }

    private fun initView() {
        binding.listRoom.apply {
//            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = roomAdapter
//            addOnScrollListener(homeViewModel.endlessScroll)
        }
    }

    private fun observer() {
        viewModel.rooms.observe(this, Observer {
            roomAdapter.setData(it)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        fun newInstance() = MeetingRoomListFragment()
    }
}