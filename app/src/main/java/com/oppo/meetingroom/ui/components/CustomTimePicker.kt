package com.oppo.meetingroom.ui.components

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.NumberPicker
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.oppo.meetingroom.R

class CustomTimePicker @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private  var edt : TextInputEditText
    private  var ly : TextInputLayout
    private val enableHour = true
    private val enableMinute = true
    private var errorEnabled = false
    private val maxHour = 23
    private val minHour = 0
    private val maxMinute = 59
    private val minMinute = 0
    private var minMinuteData: ArrayList<String> = arrayListOf()
    private var hourDate: ArrayList<String> = arrayListOf()
    private var selectHour:Int = 0
    private var selectMinute:Int = 0
    private var timeValue: String? = null
    private lateinit var timePickerListener: TimePickerListener

    init {
        val view = inflate(context, R.layout.view_custom_time_picker, this)
        edt = view.findViewById(R.id.etStartTimeCustom)
        ly = view.findViewById(R.id.lyStatTimeCustom)

        setMinMinuteValue(30)
        setHourValue(24)
        edt.setOnClickListener {
//            setTimePicker(binding.etStartTime)
            setDialog()
            ly.isErrorEnabled = false
        }

    }

    private fun setMinMinuteValue(step: Int){
        for( i in 0..59 step step){
            if(i < 10){
                minMinuteData!!.add("0$i")
            }else{
                minMinuteData!!.add("$i")
            }
        }
    }

    private fun setHourValue(hour: Int){
        for( i in 0 until hour){
            if(i < 10){
                hourDate!!.add("0$i")
            }else{
                hourDate!!.add("$i")
            }
        }
    }

    private fun setDialog() {
        val builder = AlertDialog.Builder(context)
        val inflater = LayoutInflater.from(context)
        val v: View = inflater.inflate(R.layout.dialog_custom_time_picker, null) // this line
        builder.setView(v)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        val btnSubmit = v.findViewById<TextView>(R.id.btnOk)
        val btnCancel = v.findViewById<TextView>(R.id.btnCancel)
        val nbH = v.findViewById<NumberPicker>(R.id.numberPickerHour)
        val nbM = v.findViewById<NumberPicker>(R.id.numberPickerMinute)
        nbH.isEnabled = enableHour
        nbH.minValue = minHour
        nbH.wrapSelectorWheel = true
        hourDate?.let {
            nbH.maxValue = it.size -1
            nbH.displayedValues = it.toTypedArray()
        }
        nbH.value = selectHour

        nbM.isEnabled = enableMinute
        nbM.minValue = minMinute
        minMinuteData?.let {
            nbM.maxValue = it.size-1
            nbM.displayedValues = it.toTypedArray()
        }
        nbM.value = selectMinute


        btnSubmit.setOnClickListener {
            selectHour = nbH.value
            selectMinute = nbM.value
            setTime(selectHour, selectMinute)
//            val time: String = "$selectHour:$selectMinute"
//            timePickerListener.onSelected(time)
            dialog.dismiss()
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
    }

    private fun setTime(selectHour: Int, selectMinute: Int) {
        edt.setText("${hourDate[selectHour]}:${minMinuteData[selectMinute]}")

    }

    fun setError(validSelected: Boolean, text: String? = null) {
        if (validSelected) {
            ly.error = text
            ly.isErrorEnabled = true
        } else {
            ly.error = null
            ly.isErrorEnabled = false
        }
    }

    fun checkEdEmpty(textError: String): Boolean{
        if(edt.text.isNullOrEmpty()){
            ly.isErrorEnabled = true
            ly.error = textError
            return false
        }
        ly.isErrorEnabled = false
        return true
    }

    fun getValue():String {
       return edt.text.toString()
    }


    interface TimePickerListener {
        fun onSelected(time: String?)
    }

    fun onCallback(timePickerListener: TimePickerListener) {
        this.timePickerListener = timePickerListener
    }

}
