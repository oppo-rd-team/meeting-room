package com.oppo.meetingroom.ui.booking

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.FragmentRoomForeBinding
import com.oppo.meetingroom.domain.model.Booking
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.domain.result.Response
import com.oppo.meetingroom.ui.main.MainActivity
import com.oppo.meetingroom.utli.Util
import com.oppo.meetingroom.utli.UtilDate
import com.oppo.meetingroom.utli.ext.UtilText
import com.oppo.meetingroom.utli.ext.replaceAddFragment
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*
import kotlin.concurrent.schedule


class RoomForeFragment : Fragment() {
    private lateinit var binding: FragmentRoomForeBinding
    private val roomSeletePreference: RoomSeletePreference by inject()
    private lateinit var room: Room
    private val viewModel: BookingViewModel by sharedViewModel()
    private val handler = Handler()
    private var totalTimeMinutes:Int = 0
    private var bookingActivity: Booking? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =  DataBindingUtil.inflate(inflater,R.layout.fragment_room_fore, container, false)
        binding.apply {
            lifecycleOwner = this@RoomForeFragment
        }

        initView()
        observe()
        event()
        return binding.root
    }

    private fun event() {
        binding.tvNameRoom.setOnLongClickListener {
            activity?.apply{
                MainActivity.newInstant(this)
                finish()
            }
            return@setOnLongClickListener  true
        }

        binding.floatingActionButton.setOnClickListener {
            activity?.apply {
                if(getFragmentCount() < 1){
                    replaceAddFragment(R.id.containerLeft,::RoomBookingFragment)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        init()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun observe() {
       viewModel.bookingActivity.observe(viewLifecycleOwner, Observer {
           when(it){
               is Response.Success -> {
                   bookingActivity?.let { it1 ->
                       if(it1.id != it.data.id){
                           bookingActivity = it.data
                           setTime()
                       }else{
                           bookingActivity = it.data
                       }
                   } ?: apply {
                       bookingActivity = it.data
                       setTime()
                   }
                   bookingActivity?.let { it ->
                       totalValueTime(it.datetimeFrom,it.datetimeTo)
                   }
               }
               Response.Empty -> {
                   binding.status.text = "Available"
                   binding.status.backgroundTintList = context?.resources?.getColorStateList(R.color.approve)
                   binding.tvTime.setTextColor(ContextCompat.getColor(requireContext(),R.color.green_500));
               }
               else -> {}
           }
       })
    }

    private fun initView() {
        roomSeletePreference.getRoom()?.let {
            binding.tvNameRoom.text = it.roomName
            setImage(binding.image,it)
        }
    }

    private fun init() {
        roomSeletePreference.getRoom()?.let {
            this.room = it
        }
        viewModel.fetchListBooking(room, requireActivity())
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setTime(){
        handler.post(object : Runnable {
            override fun run() {
                handler.postDelayed(this, 1000)
                bookingActivity?.let {
                    binding.tvTime.setTextColor(ContextCompat.getColor(requireContext(), R.color.red));
                    binding.status.text = "Reserved"
                    binding.status.backgroundTintList = context?.resources?.getColorStateList(R.color.red)
                    updateTime(it.datetimeTo)
                }
            }
        })
    }

    fun updateTime(endDate: String) {
        // Set Current Date
        val currentDate = Calendar.getInstance()

        // Set Event Date
        val eventDate = UtilDate.getDataCalendar(endDate)

        // Find how many milliseconds until the event
        val diff = eventDate.timeInMillis - currentDate.timeInMillis

        // Change the milliseconds to days, hours, minutes and seconds
        val days = diff / (24 * 60 * 60 * 1000)
        val hours = diff / (1000 * 60 * 60) % 24
        val minutes = diff / (1000 * 60) % 60
        val seconds = (diff / 1000) % 60

        setCircular(hours,minutes)
        binding.tvTime.text = UtilText.textTime(hours,minutes,seconds)
        endEvent(currentDate, eventDate)

    }

    private fun setCircular(hours: Long, minutes: Long){
        val convertHours = if(hours > 0){ hours * 60 }else{ 0 }
        val sum = convertHours + minutes
        val valueCircular = (sum.toDouble() / totalTimeMinutes.toDouble()) * 100
        binding.circular.setProgress(valueCircular.toInt())
    }

    private fun totalValueTime(startDate: String, endDate: String){
        // Set Event Date
        val eventDate = UtilDate.getDataCalendar(endDate)
        val startDate = UtilDate.getDataCalendar(startDate)

        val diff = eventDate.timeInMillis - startDate.timeInMillis

        val days = diff / (24 * 60 * 60 * 1000)
        val hours = diff / (1000 * 60 * 60) % 24
        val minutes = diff / (1000 * 60) % 60
        val seconds = (diff / 1000) % 60

        val convertHours = if(hours > 0){ hours * 60 }else{ 0 }
        totalTimeMinutes = convertHours.toInt() + minutes.toInt()
    }

    @SuppressLint("ResourceAsColor")
    private fun endEvent(currentdate: Calendar, eventdate: Calendar) {
        if (currentdate.time >= eventdate.time) {
            handler.removeMessages(0)
            Timer().schedule(5000) {
                viewModel.fetchBookingActive(requireContext(), room)
            }
        }
    }

    private fun setImage(view: ImageView, item: Room){
//        Picasso.get().load(item.roomPhoto).into(view)
//        Util.imageNoView(requireContext(), item.roomPhoto, view)
        Glide.with(requireContext())
            .asBitmap()
//                            .apply(RequestOptions().override(100,100))
            .load(item.roomPhoto)
            .into(object : CustomTarget<Bitmap>(){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    view.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })
    }

    private fun getFragmentCount(): Int {
        return   requireActivity().supportFragmentManager.backStackEntryCount
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeMessages(0)
    }


    companion object {
        @JvmStatic
        fun newInstance() = RoomForeFragment()
    }
}