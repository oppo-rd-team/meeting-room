package com.oppo.meetingroom.ui.booking

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.oppo.meetingroom.R
import com.oppo.meetingroom.databinding.FragmentRoomBookingListBinding
import com.oppo.meetingroom.domain.model.Booking
import com.oppo.meetingroom.domain.model.Room
import com.oppo.meetingroom.domain.preferences.RoomSeletePreference
import com.oppo.meetingroom.domain.result.Response
import com.oppo.meetingroom.ui.booking.adapter.BookingListAdapter
import com.oppo.meetingroom.utli.Util
import com.oppo.meetingroom.utli.UtilDate
import com.oppo.meetingroom.utli.ext.replaceAddFragment
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RoomBookingListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
open class RoomBookingListFragment : Fragment() {

    private lateinit var binding: FragmentRoomBookingListBinding
    private val viewModel: BookingViewModel by sharedViewModel()
    private val roomSeletePreference : RoomSeletePreference by inject()
    private lateinit var bookingListAdapter: BookingListAdapter
    private lateinit var room: Room
    private var bookingActivity: Booking? = null
    private lateinit var bookingList: List<Booking>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_room_booking_list, container, false)
        init()
        initView()
        observer()
        event()

        return binding.root
    }

    private fun initView() {
        binding.bookingList.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = bookingListAdapter
        }
    }

    private fun init() {
        bookingListAdapter = BookingListAdapter(requireContext(),viewModel)
//        roomSeletePreference.getRoom()?.let {
//            this.room = it
//        }
//        viewModel.fetchListBooking(requireContext(),UtilDate.getDataNow(),room)
    }

    private fun observer() {
        viewModel.bookingActivity.observe(this, Observer {
            when(it){
                is Response.Success -> {
                    bookingActivity = it.data
                }
                Response.Empty -> {
                    bookingActivity = null
                }
                else -> {}
            }
        })
        viewModel.booking.observe(this, Observer {
            when(it){
                is Response.Success -> {
                    if(it.data.isEmpty()){
//                        binding.reservedTime.visibility = View.GONE
                        binding.tvEmpty.visibility = View.VISIBLE
                    }else{
//                        binding.reservedTime.visibility = View.VISIBLE
                        binding.tvEmpty.visibility = View.GONE
                    }
                    bookingList = it.data
                    setDataToList(it.data)
                }
                else -> {}
            }
        })
    }


    private fun event(){

    }

    private fun setDataToList(list: List<Booking>) {
        bookingActivity?.let {
//            var newList = removeItemBookingList(list, it)
            bookingListAdapter.setData(checkListToCurrentTime(list),true)
//            bookingListAdapter.setData(list,true)

        } ?: apply {
            bookingListAdapter.setData(checkListToCurrentTime(list),false)
        }
    }

    private fun removeItemBookingList(bookingList: List<Booking>, bookingActive: Booking): List<Booking> {
        var list = bookingList.toMutableList()
        var i = 0
        bookingList.forEachIndexed { index, booking->
            if(booking.id == bookingActive.id) {
                i = index
            }
        }
        list.removeAt(i)
        list.add(0,bookingActive)
        return list
    }

    private fun checkListToCurrentTime(bookingList: List<Booking>): List<Booking> {
        val currentDate = Calendar.getInstance()
        var listT = bookingList.toMutableList()
        bookingList.forEachIndexed { index, booking ->
            if(UtilDate.getDataCalendar(booking.datetimeTo).time <= currentDate.time){
                listT.remove(booking)
            }
        }

        if(listT.size == 0){
            binding.tvEmpty.visibility = View.VISIBLE
        }else{
            binding.tvEmpty.visibility = View.GONE
        }
        return listT

    }

    private fun getFragmentCount(): Int {
        return   requireActivity().supportFragmentManager.backStackEntryCount
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = RoomBookingListFragment()
    }
}