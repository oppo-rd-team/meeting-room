package com.oppo.meetingroom.utli

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.oppo.meetingroom.R


class FirebaseConfig {
    var mFirebaseRemoteConfig: FirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
    private val configSettings: FirebaseRemoteConfigSettings = FirebaseRemoteConfigSettings.Builder()
        .build()
    private var cacheExpiration: Long = 0
    var password = ""

    var _password: MutableLiveData<String> = MutableLiveData()

    init {

        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings)
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
    }

    fun fetchNewPassword(context: Context?): String {
        mFirebaseRemoteConfig.fetch(getCacheExpiration())
            .addOnCompleteListener(context as Activity
            ) { task -> // If is successful, activated fetched
                if (task.isSuccessful) {
                    mFirebaseRemoteConfig.fetchAndActivate()
                } else {
                }
                password = mFirebaseRemoteConfig.getString("password")
                _password.value = password
            }
        return password
    }

    fun remote() : FirebaseRemoteConfig {
        return mFirebaseRemoteConfig
    }

    fun loadPassword() : LiveData<String> = _password

    private fun getCacheExpiration(): Long {
// If is developer mode, cache expiration set to 0, in order to test
//        if (mFirebaseRemoteConfig.info.configSettings.is) {
//            cacheExpiration = 0
//        }

        return cacheExpiration
    }
}