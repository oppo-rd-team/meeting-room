package com.oppo.meetingroom.utli

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import com.oppo.meetingroom.ui.components.BaseDialogFragment
import org.json.JSONObject
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class UtilDate {
    companion object{
        fun getDataNow(): String{
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            return sdf.format(Date())
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                val current = LocalDateTime.now()
//
//                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
//                return  current.format(formatter)
//            } else {
//                val sdf =
//                    SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
//                return sdf.format(Date())
//            }
        }

        fun getDataNowMM(): String{
            val sdf = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            return sdf.format(Date())
        }

        fun getDataNowYyyyMmDd(): String{
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            return sdf.format(Date())
        }

        fun getDataTimeNow(): String{
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            return sdf.format(Date())
        }

        fun coverDateToTime(date: String):String {
            val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val formatter = SimpleDateFormat("HH:mm")
            return formatter.format(parser.parse(date))
        }

        fun coverDateTimeToYMD(date: String, time: String):String {
            val newDate = "$date $time"
            val parser = SimpleDateFormat("dd MMM yyyy HH:mm")
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            return formatter.format(parser.parse(newDate))
        }

        fun coverDateTimeToDDMMYY(date: String):String {
            val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val formatter = SimpleDateFormat("dd MMM yyyy")
            return formatter.format(parser.parse(date))
        }

        fun splitData(dateTime: String): String {
            val f: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val d = f.parse(dateTime)
            val date: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            return date.format(d)
            val time: DateFormat = SimpleDateFormat("HH:mm:ss")
        }

        fun splitTime(dateTime: String): String {
            val f: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val d = f.parse(dateTime)
            val time: DateFormat = SimpleDateFormat("HH:mm:ss")
            return time.format(d)
        }

        fun convertFormatDateMa(date: String): String {
            val parser = SimpleDateFormat("dd MM yyyy")
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            return formatter.format(parser.parse(date))
        }

        fun splitDateTime(dateTime: String): JSONObject{
            var date = splitData(dateTime)
            var time = splitTime(dateTime)
            val dataS = date.split("-").toTypedArray()
            val timeS = time.split(":").toTypedArray()

            val rootObject= JSONObject()
            rootObject.put("y",dataS[0])
            rootObject.put("m",dataS[1])
            rootObject.put("d",dataS[2])
            rootObject.put("h",timeS[0])
            rootObject.put("m",timeS[1])
            rootObject.put("s",timeS[2])

            Log.d("y",rootObject.getString("y"))
            Log.d("m",rootObject.getString("m"))
            Log.d("d",rootObject.getString("d"))
            Log.d("h",rootObject.getString("h"))
            Log.d("m",rootObject.getString("m"))
            return rootObject
        }

        fun getDataCalendar(DateTime:String): Calendar{
            val eventDate = Calendar.getInstance()
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            eventDate.time = sdf.parse(DateTime)
            return eventDate
            //        eventDate[Calendar.YEAR] = dataS.getString("y").toInt()
            //        eventDate[Calendar.MONTH] = dataS.getString("m").toLong()-1
            //        eventDate[Calendar.DAY_OF_MONTH] = dataS.getString("d").toInt()
            //        eventDate[Calendar.HOUR_OF_DAY] = dataS.getString("h").toInt()
            //        eventDate[Calendar.MINUTE] = dataS.getString("m").toInt()
            //        eventDate[Calendar.SECOND] = dataS.getString("s").toInt()
            //        eventDate.timeZone = TimeZone.getTimeZone("Asia/Bangkok")
        }

    }
}