package com.oppo.meetingroom.utli.ext

class UtilText {
    companion object{
        fun textTime(hours:Long,minutes: Long,seconds:Long):String {
            var txtHours = "$hours"
            var txtM = "$minutes"
            var txtS = "$seconds"
            if(hours < 10){
                txtHours = "0$hours"
            }
            if(minutes < 10){
                txtM = "0$minutes"
            }
            if(seconds < 10){
                txtS = "0$seconds"
            }
            return "${txtHours}:${txtM}:${txtS}"
        }
    }
}