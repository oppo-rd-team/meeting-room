package com.oppo.meetingroom.utli

import android.app.Activity
import android.content.Context
import android.util.Base64
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.oppo.meetingroom.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class Util {
    companion object{
        fun decode(s: String): String {
            val RTRIM = Pattern.compile("=")
            val encodeValue = Base64.encodeToString(s.toByteArray(), Base64.NO_PADDING)
            val str = encodeValue.replace("+/", "-_")
            return RTRIM.matcher(str).replaceAll("")
        }

        fun datediff(dates: String?, datee: String?): Int {
            val diff: Long = stringToDate(dates)!!.getTime() - stringToDate(datee)!!.getTime()
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            return days.toInt()
        }

        fun stringToDate(dateInput: String?): Date? {
            val format = SimpleDateFormat("yyyy-MM-dd")
            return try {
                format.parse(dateInput)
            } catch (e: ParseException) {
                e.printStackTrace()
                null
            }
        }
        fun today(): String {
            return SimpleDateFormat("yyyy-MM-dd").format(Date())
        }

        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager: InputMethodManager = activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken, 0
            )
        }

        fun hideSoftKeyboard(view: View, activity: Activity) {
            val inputMethodManager: InputMethodManager = activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                view.windowToken, 0
            )
        }

        fun imageNoView(context: Context?, path: String?, imageView: ImageView?) {
            Glide.with(context!!)
                .load(path)
                .apply(
                    RequestOptions()
                        .error(R.drawable.icon_oppo_black)
                        .override(768, 1024)
                        .placeholder(progressDrawable(context))
                        .skipMemoryCache(false)
                ).into(imageView!!)
        }

        private fun progressDrawable(context: Context): CircularProgressDrawable {
            val circularProgressDrawable =
                CircularProgressDrawable(context)
            circularProgressDrawable.setStrokeWidth(7f)
            circularProgressDrawable.setCenterRadius(30f)
            circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(context, R.color.black))
            circularProgressDrawable.start()
            return circularProgressDrawable
        }

    }
}