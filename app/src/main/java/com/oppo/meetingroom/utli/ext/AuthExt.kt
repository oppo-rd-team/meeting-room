package com.oppo.meetingroom.utli.ext

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.util.Log
import com.oppo.meetingroom.domain.IoppoService
import com.oppo.meetingroom.domain.Remote
import com.oppo.meetingroom.domain.model.User
import com.oppo.meetingroom.domain.preferences.UserPreference
import com.oppo.meetingroom.utli.FirebaseConfig
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.HttpException

@SuppressLint("CheckResult")
fun refreshToken(
    service: IoppoService,
    preference: UserPreference,
    fail: Throwable,
    f: () -> Unit,
    error: (Throwable) -> Unit,
    context: Context
) {
    if (fail is HttpException && (fail.code() == 304 || fail.code() == 401)) {
//        val password = FirebaseConfig().fetchNewPassword(context)
        FirebaseConfig().remote().fetch(0)
            .addOnCompleteListener(
                context as Activity
            ) { task -> // If is successful, activated fetched
                if (task.isSuccessful) {
                    FirebaseConfig().remote().fetchAndActivate()
                } else {
                    Log.d("TAG", "=> fail")
                }
                val password = FirebaseConfig().remote().getString("password")

                preference.setAuth(User(password = password))
                preference.getUser()?.let { user ->
                    service.login(user.source, user.staffCode, user.password)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            Remote.token = it.token
                            preference.setAuth(User(token = it.token))
                            f()
                        }, {
                            error(fail)
                        })
                } ?: error(fail)
            }
    } else {
        error(fail)
    }
}
