package com.oppo.meetingroom.utli.ext

import com.google.gson.Gson
import com.oppo.meetingroom.domain.model.Error
import retrofit2.HttpException
import java.net.UnknownHostException
import java.util.*


fun Throwable.handleErrorBody(): Exception {
    return when (this) {
        is HttpException -> {
            this.response()?.errorBody()?.let {
                val message = StringBuilder()
                if (message.isEmpty()) {
                    message.append(this)
                }
                Exception(message.toString())

            } ?: run {
                Exception(this)
            }
        }
        is UnknownHostException -> {
            Exception("ขออภัย ไม่พบสัญญานอินเตอร์เน็ต")
        }
        else -> Exception(this.message)
    }
}
