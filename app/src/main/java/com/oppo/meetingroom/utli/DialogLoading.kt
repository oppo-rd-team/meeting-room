package com.oppo.meetingroom.utli

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ThreeBounce
import com.oppo.meetingroom.R

class DialogLoading {
    companion object{
        private lateinit var dialog: Dialog
        private var checkload = 0
        fun showDialogLoading(context: Context?) {
            if (checkload == 1) {
                dialog.dismiss()
                checkload = 0
            }
            dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.loading)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //set to transparent
            val progressBar: ProgressBar = dialog.findViewById(R.id.spin_kit)
            val relative: LinearLayout = dialog.findViewById(R.id.main_li)
            val loading: Sprite = ThreeBounce()
            progressBar.indeterminateDrawable = loading
            checkload = 1
            dialog.show()
        }

        fun closeDialogLoading(context: Context?) {
            checkload = 0
            if (dialog != null) {
                dialog.dismiss()
            }
        }
    }
}